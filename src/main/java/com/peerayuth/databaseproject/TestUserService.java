/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peerayuth.databaseproject;

import com.peerayuth.databaseproject.dao.UserDao;
import com.peerayuth.databaseproject.model.User;
import com.peerayuth.databaseproject.service.UserService;

/**
 *
 * @author Ow
 */
public class TestUserService {
    public static void main(String[] args) {
        UserService userSevice = new UserService();
        User user = userSevice.login("user2", "password1");
        if(user != null) {
            System.out.println("Welcome user : "+ user.getName());
        } else {
            System.out.println("Error");
        }
        
    }
    
}
